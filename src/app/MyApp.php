<?php

use Eole\Sandstone\Application;
use Eole\Sandstone\Serializer\ServiceProvider as SerializerProvider;
use Eole\Sandstone\Websocket\ServiceProvider as WebsocketProvider;
use Eole\Sandstone\Push\ServiceProvider as PushServiceProvider;
use Eole\Sandstone\Push\Bridge\ZMQ\ServiceProvider as ZMQServiceProvider;

class MyApp extends Application
{
    public function __construct()
    {
        parent::__construct([
            'debug' => true,
        ]);

        $this->register(new SerializerProvider());

        $this->register(new WebsocketProvider(), [
            'sandstone.websocket.server' => [
                'bind' => '0.0.0.0',
                'port' => '25569',
            ],
        ]);

        $this->register(new PushServiceProvider());

        $this->register(new ZMQServiceProvider(), [
            'sandstone.push.server' => [
                'bind' => '127.0.0.1',
                'host' => '127.0.0.1',
                'port' => 5555,
            ],
        ]);

        $this['serializer.builder']->addMetadataDir(__DIR__, '');
    }

}