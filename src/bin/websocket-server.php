<?php

require dirname(__DIR__) . '/vendor/autoload.php';

use Eole\Sandstone\Websocket\Server as WebsocketServer;

$app = new MyApp();

$app->topic('chat/{channel}', function ($topic_pattern) {
        return new ChatTopic($topic_pattern);
    })
    ->value('channel', 'general')
    ->assert('channel', '^[a-zA-Z0-9]+$');

$websocketServer = new WebsocketServer($app);

$websocketServer->run();
